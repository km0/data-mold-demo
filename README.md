# Data Molding

_Application abstract for mur.at open call_

There is something profoundly different between the organic and digital worlds. A detail that, left out, radically influences the way we think and interact with data. Data don't go bad. They don't rot. They may get corrupted, but they don't decompose. They become inaccessible, unsupported, or obsolete, but in a sterile way: inert material, not prolific compost.

Inspired by mould and its transformative role in natural waste, `Data Molding` is a project that attempts to incorporate ecological thinking in the development of digital tools. In its double meaning, the title evokes a destructive and decadent force (to mold as in to mould), and simultaneously a generative impulse of creation (to mold as in to shape). Modulating this balance can make different forms of experimentation germinate: a tool as an ecological niche, a tool as an ecosystem, a tool as a garden.

Here we propose the `data mold`, a digital tool to activate the mur.at archive, and interact both with its contents and its structure. A way to mold the archive, and mold some principles into its "nature". It acknowledges that an archive is a constant transformation, and its taxonomy always being questioned. It facilitates and invites several outcomes, making space for diverse perspectives, for akin and contradictory versions, and for messy yet expressive processes.

A `data mold` is an interactive environment with a series of hyphae-like forms that slowly grow and branch out through space. These roots are composed of ever-mutating QR codes, that allow for localised interactions with the organism. By framing a QR code, one can interact with that specific part of the mold. It can be used not only for accessing the QR content, but also influencing behavior and evolution of that branch. The tree filesystem becomes a mould filesystem. The branches can link to contents, but also work as containers where to deposit materials.

Here two practical examples of activation of a data-mold. Imagine it with a wall projection, and people interacting with their smartphone.

-   A workshop setup to explore and curate the mur.at archive. The data-mold offers spatial organisation of textual, visual and audio material, where each QR becomes a place of data storage and aggregation. By interacting with a block, it is possible to leave an image or text, that can be displayed right next to the QR. Content invites new content, exploring thematic, associative and curatorial forms. Imagine repeating the workshop with different participants: even starting from the same material the resulting data-mold will look totally different.

-   An exhibition setup lasting several days. Interactions can influence the growth of moulds: by interacting with a part of the organism, one could come into contact with spores, which could then be deposited in subsequent interactions. Those who activate the installation become like pollinators, who attracted by the shapes and contents influence the development of the ecosystem. Transformations can target the behaviour and form in which this digital mycorrhiza grows and its attributes: the type of content it hosts, its quality, size and format. Imagine a parametric max-size for file upload! One branch might host only videos, and gradually evolve to demand smaller and smaller compressed files. Another branch developed with only textual content might join one that accepts only images, and then continue by requiring images with a textual description. And so on.

A data-mold can be activated in different ways. True to the principles of situated software, it rejects the idea of single and universal tool. It sprouts instead from the needs and sensibility of different hosting communities. In this regard, the development of Data Molding is structured through a series of different appointments, aimed at exploring the tool in different situations.

For this residency we can imagine a workshop format and an exhibition setup.

An exploration of data-molds in performative context is planned later in summer, and a focus on spatial interaction and choreography the following winter.
